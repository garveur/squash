*** Settings ***
Resource	squash_resources.resource


*** Test Cases ***

BDD9_Case_01_HealthcareProgram_1
	[Setup]	Test Setup

	Given L'utilisateur est sur la page d'accueil
	    When L'utilisateur souhaite prendre un rendez-vous
	            Then La page de connexion s'affiche
	    When L'utilisateur se connecte
	            Then L'utilisateur est connecté sur la page de rendez-vous
	    When Je renseigne les données obligatoires
	        And Je choisis la clinique    ${jdd1.facility}
	        And Je choisis HealthcareProgram    ${jdd1.HealthcareProgram}
            And Je clique sur Book Appointment
	            Then Le rendez-vous est confirmé
	                And La clinique est    ${jdd1.facility}
	                And Le HealthcareProgram est    ${jdd1.VerifHealthcareProgram}

	[Teardown]	Test Teardown
BDD9_Case_01_HealthcareProgram_2
	[Setup]	Test Setup

	Given L'utilisateur est sur la page d'accueil
	    When L'utilisateur souhaite prendre un rendez-vous
        		Then La page de connexion s'affiche
		When L'utilisateur se connecte
		        Then L'utilisateur est connecté sur la page de rendez-vous
		when When Je renseigne les données obligatoires
		    And Je choisis la clinique    ${jdd2.facility}
		    And Je choisis HealthcareProgram    ${jdd2.HealthcareProgram}
		    And Je clique sur Book Appointment
		        Then Le rendez-vous est confirmé
		            And La clinique est    ${jdd2.facility}
		            And Le HealthcareProgram est    ${jdd2.VerifHealthcareProgram}

	[Teardown]	Test Teardown

BDD9_Case_01_HealthcareProgram_3
	[Setup]	Test Setup

	Given L'utilisateur est sur la page d'accueil
	    When L'utilisateur souhaite prendre un rendez-vous
	            Then La page de connexion s'affiche
	    When L'utilisateur se connecte
	            Then L'utilisateur est connecté sur la page de rendez-vous
	    When Je renseigne les données obligatoires
	        And Je choisis la clinique    ${jdd3.facility}
	        And Je choisis HealthcareProgram    ${jdd3.HealthcareProgram}
            And Je clique sur Book Appointment
	       	    Then Le rendez-vous est confirmé
	                And La clinique est    ${jdd3.facility}
	                And Le HealthcareProgram est    ${jdd3.VerifHealthcareProgram}

	[Teardown]	Test Teardown